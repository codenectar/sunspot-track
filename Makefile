# Makefile for sunspot-track

SLUG = sunspot-track
REGISTRY = registry.gitlab.com
PROJECT_PATH = codenectar/$(SLUG)
IMAGE = $(REGISTRY)/$(PROJECT_PATH)/$(SLUG)
DOCKER_VOLUME = cocalc-projects
IMAGES_PATH = "$(shell pwd)/image/"


default: serve

clean:
	docker image rm --force $(IMAGE)

build:
	docker build --tag $(IMAGE) "$(shell pwd)"

# Run the Docker container as daemon,
# port 443 available as port 8443 on the host,
# Docker volume mounted at /projects,
# images mounted at /workdir/image,
# and then watch the logs

serve: build
	docker run --detach \
	           --name $(SLUG) \
	           --publish 8443:443 \
		   --volume ${DOCKER_VOLUME}:/projects \
		   --volume ${IMAGES_PATH}:/workdir/image \
	           $(IMAGE)
	docker logs $(SLUG) -f

# Note: for the 'push' target to succeed you must have already authenticated to
# the registry with the command:
#     $ docker login -u $(TOKEN_NAME) $(REGISTRY)
# You will need to supply credentials (e.g. valid deploy token name and secret)

push:
	docker image push $(IMAGE)

