#!/usr/bin/env bash

# start-cocalc.sh

DOCKER_VOLUME="cocalc-projects"

function volumeExists {
    if [ "$(docker volume ls -f name=$1 | awk '{print $NF}' | grep -E '^'$1'$')" ]
    then
        return 0
    else
        return 1
    fi
}

if volumeExists ${DOCKER_VOLUME}; then
    echo "${DOCKER_VOLUME} exists: using it"
else
    echo "${DOCKER_VOLUME} does not exist: creating it"
    docker volume create ${DOCKER_VOLUME}
fi

docker run --name=cocalc -d -p 443:443 \
    -v ${DOCKER_VOLUME}:/projects \
    -v ./image/:/workdir/image/ \
    sagemathinc/cocalc-v2

docker logs cocalc -f

