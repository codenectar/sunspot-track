#!/usr/bin/env python3

# sun-center.py -- given a GIF image of the sun that has been thresholded
#                  returns the center and radius of solar disc, in pixels
#
# warning: this makes a (bad) assumption that pixels will all be black in
#          the background of the image

import argparse
from PIL import Image

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('sunfile', help='the sun image to measure')
    parser.add_argument('--verbose', '-v', action='count', help='be verbose')
    args = parser.parse_args()

    # read the image file into memory
    with Image.open(args.sunfile) as im:
        px = im.load()

    # scan the image to find the diameter of the solar disc (pixels)
    (width, height) = im.size
    print(f'The image is {width} pixels wide and {height} pixels tall')
    diam_y = 0              # row containing max disc width (the diameter)
    max_w = 0               # max disc width found so far
    widest_row = 0          # store the row of the widest part of solar disc
    widest_x_min = None     # store the left extent of solar disc
    widest_x_max = None     # store the right extent of solar disc
    for y in range(height):   # process all rows in image
        deltas = []           # record positions of change from one color to the other
        level = 0             # current color level we are following
        for x in range(width):    # process all pixels in the row
            z = px[x, y]          # current pixel color
            if z != level:        # color change?
                deltas.append(x)  # record the location
                level = z         # change the color we are following
        if args.verbose:
            print(f'ROW {y} DELTAS {deltas}')
        if deltas:                      # if there were color changes in this row...
            w = deltas[-1] - deltas[0]  # measure distance from first to last
            if args.verbose:
                print(f'    width = {w}')
            if w > max_w:               # if wider then previous maximum...
                max_w = w               # update maximum width
                widest_row = y          # remember this row as widest part of disc (so far)
                widest_x_min = deltas[0]   # remember leftmost pixel in disc
                widest_x_max = deltas[-1]  # remember rightmost pixel in disc
    radius = int(max_w / 2 + 0.5)
    print(f'The radius of the Sun is {radius} pixels')
    center_x = widest_x_min + radius
    center_y = widest_row
    print(f'The center of the Sun is at ({center_x},{center_y})')
