import glob
import IPython.display
import ipywidgets

path = ipywidgets.Text(description='String:', value='/workdir/image/original')

options = glob.glob('{}/*'.format(path.value))
files = ipywidgets.Select(description='Choose Image', options=options)

IPython.display.display(files)

import skimage

im_gray = Image.open(files.value).convert('L')
np_gray = numpy.array(im_gray)
otsu_threshold = skimage.filters.threshold_otsu(np_gray)
np_disc = np_gray > otsu_threshold

im_disc = Image.fromarray(np_disc)
display(im_disc)

from scipy import ndimage

center = ndimage.center_of_mass(np_disc)
percent = numpy.divide(center, np_gray.shape)

# draw crosshairs on the disc
np_disc[int(center[0] + 0.5), : ] = False
np_disc[ :, int(center[1] + 0.5)] = False
im_disc = Image.fromarray(np_disc)
display(im_disc)
