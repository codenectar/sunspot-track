# Sunspot Track

This project processes digital image of the Sun that contain sunspots.

# Threshold

A series of black-and-white images are produced by thresholding the original
image at various levels.  With a low threshold (for example 20%) the sunspots
disappear but the disc of the Sun becomes very circular and well-defined.  At
higher thresholds (depending on the exposure of the photo) the sunspots begin
to appear, but at the same time the solar disc starts to become "eroded" by
the high threshold.

The script that performs this task is
[threshold.sh](https://gitlab.com/codenectar/sunspot-track/-/blob/main/threshold.sh)

# Calculate a Delta Image

Use [disc-to-delta.py](https://gitlab.com/codenectar/sunspot-track/-/blob/main/disc-to-delta.py)
to convert the low-threshold solar disc image into a ring that follows the edge of the solar
disc, plus a few wayward specks here and there from imperfections like dust on the image.  The
ring should be distinct and sharp.

# Calculate Center and Radius of the Solar Disc

Using a low-threshold image with its well-defined solar disc we can find the center
of the Sun in the image and its radius.  The script that performs this task is
[sun-center.py](https://gitlab.com/codenectar/sunspot-track/-/blob/main/sun-center.py)

# Find Sunspots in the Image

Using a high- (but not TOO high) threshold image with its well-defined sunspots
(and fuzzier solar disc) we can find the angle and distance from the center of the
disc (determined by the script above) to each of the pixels in sunspots.

The script that performs this task is
[spiral-to-spots.py](https://gitlab.com/codenectar/sunspot-track/-/blob/main/spiral-to-spots.py)

Note that this script can produce a lot of output: it will output a co-ordinate for every
black pixel in the B+W image that is within the radius you give it from the center point
that you give it.  So it is important to choose a threshold level that just barely shows
the sunspots.

