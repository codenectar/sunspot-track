# Dockerfile for sunspot-track

FROM sagemathinc/cocalc-v2:latest

# get the latest Debian patches
#RUN apt update && apt -y upgrade

# install required python libraries into container
RUN pip3 install --upgrade --no-cache-dir scikit-image

