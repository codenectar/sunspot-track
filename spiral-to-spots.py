#!/usr/bin/env python3

# spiral-to-spots.py -- given a GIF image of the Sun that has been thresholded to show sunspots,
#                       and the center and radius of the Sun (obtained previously from sun-center.py),
#                       returns angular coordinates of sunspots found within the radius.

import math
import argparse
from PIL import Image

def direction(x1, y1, x2, y2):
    '''the direction from (x1, y1) to (x2, y2)'''
    '''negative values are upward, positive values downward
       zero to ninety is rightward, over ninety is leftward
    '''
    delta_x = x2 - x1
    delta_y = y2 - y1
    radians = math.atan2(delta_y, delta_x)
    return math.degrees(radians)

def spiral(start_x, start_y, max_x, max_y):
    '''generate a spiral of successive rectangular rings within bounding box'''
    max_ring = min(start_x, start_y, max_x - start_x, max_y - start_y)
    yield (0,0)
    for ring in range(1, max_ring + 1):
        dy = -ring
        for dx in range(-ring, ring):
            yield (dx,dy)
        dx = ring
        for dy in range(-ring, ring):
            yield (dx,dy)
        dy = ring
        for dx in range(ring, -ring, -1):
            yield (dx,dy)
        dx = -ring
        for dy in range(ring, -ring, -1):
            yield (dx,dy)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('sunfile', help='the sun image to measure')
    parser.add_argument('center',  help='the location (x,y pixels) of the center of the sun')
    parser.add_argument('radius',  help='the radius (pixels) to search')
    args = parser.parse_args()
    center_list = args.center.split(',')
    center_x = int(center_list[0])
    center_y = int(center_list[1])
    radius = int(args.radius)

    # read the image file into memory
    with Image.open(args.sunfile) as im:
        px = im.load()
    (im_width, im_height) = im.size

    # spiral out from center looking for sunspots
    for delta in spiral(center_x, center_y, im_width - 1, im_height - 1):
        x = center_x + delta[0]
        y = center_y + delta[1]
        d = math.dist([x, y], [center_x, center_y])
        if d > radius:
            continue
        if (px[x,y] == 0):
            theta = direction(center_x, center_y, x, y)
            print(f'Distance: {d:.2f}, Angle: {theta:.2f}')
