#!/usr/bin/env bash

# threshold.sh

if [ $# -lt 1 ]
then
    echo "Usage: $0 image-path [threshold-percent]"
    exit
fi
IMAGEPATH="$1"
THRESHOLD="$2"

# strip off any directory prefix from the pathname
FILENAME=$(basename -- "$IMAGEPATH")
echo "Processing $FILENAME ..."

# split out the prefix and extension parts of the filename
PREFIX="${FILENAME%.*}"
EXT="${FILENAME##*.}"

#convert "$BASENAME" -colors 2 -threshold $THRESHOLD "$OUTFILE"
#convert "$BASENAME" -dither None "$OUTFILE"
#convert "$BASENAME" -colorspace Gray "$OUTFILE"
#convert "$BASENAME" -threshold $THRESHOLD "$OUTFILE"

if [ $# -lt 2 ]
then
    for threshold in 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90
    do
        OUTFILE="$PREFIX-thresh-${threshold}.gif"
        echo "... $OUTFILE"
        convert "$IMAGEPATH" -threshold ${threshold}% "$OUTFILE"
    done
else
    OUTFILE="$PREFIX-thresh-${THRESHOLD}.gif"
    echo "... $OUTFILE"
    convert "$IMAGEPATH" -threshold ${THRESHOLD}% "$OUTFILE"
fi

