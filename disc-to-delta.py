#!/usr/bin/env python3

import os
import sys
import numpy
import argparse
from PIL import Image

def write_now(s):
    sys.stdout.write(s)
    sys.stdout.flush()

def main(discfile, verbose=False):
    root, ext = os.path.splitext(discfile)

    # read the image file into memory
    with Image.open(discfile) as im_in:
        arr_in = im_in.load()
    (im_width, im_height) = im_in.size
    if verbose:
        print(im_in.size)

    # create a new image array to hold result
    arr = numpy.empty(im_in.size, dtype='B')

    # populate the array with delta values
    for x in range(im_width):
        if not x % 100:
            if verbose:
                write_now('.')
        this_pt = arr_in[x, 0]
        for y in range(im_height - 1):
            next_pt = arr_in[x, y + 1]
            delta = next_pt - this_pt
            this_pt = next_pt
            if delta != 0:
                arr[x,y] = 0xFF
            else:
                arr[x,y] = 0x00
        arr[x,im_height - 1] = 0x00
    if verbose:
        print()
    out_arr = numpy.transpose(arr)

    # write the new image
    im_out = Image.fromarray(out_arr, mode='P')
    if verbose:
        print(im_out.size)
    im_out.save(f'{root}-delta{ext}')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('discfile', help='the thresholded solar disc image')
    parser.add_argument('--verbose', '-v', action='count', help='be verbose')
    args = parser.parse_args()
    main(args.discfile, verbose=args.verbose)
