#!/usr/bin/env python3

# delta-to-ellipse.py

import argparse
import ellipser
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

def readfile(deltafile, verbose=False):

    # read the image file into memory
    with Image.open(args.deltafile) as im_in:
        arr_in = im_in.load()

    (im_width, im_height) = im_in.size
    if verbose:
        print(f'File dimensions are {im_in.size}')
    x_seq = []
    y_seq = []
    for x in range(im_width):
        for y in range(im_height):
            if arr_in[x,y] > 0:
                x_seq.append(x)
                y_seq.append(y)
    return x_seq, y_seq

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('deltafile', help='the delta file to start with')
    parser.add_argument('--plot', '-p', action='count', help='plot the results')
    parser.add_argument('--verbose', '-v', action='count', help='be verbose')
    args = parser.parse_args()

    # create numpy arrays
    print(f'Filename: {args.deltafile}')
    x_seq, y_seq = readfile(args.deltafile, verbose=args.verbose)
    x = np.array(x_seq)
    y = np.array(y_seq)
    if args.verbose:
        print(f'Delta points: {x.shape}, {y.shape}')

    # calculate ellipse
    coeffs = ellipser.fit_ellipse(x, y)
    print('F(x, y) = ax^2 + bxy + cy^2 + dx + ey + f = 0')
    print('a, b, c, d, e, f =', coeffs)
    x0, y0, ap, bp, e, phi = ellipser.cart_to_pol(coeffs)
    print('Ellipse parameters')
    print('x0, y0, ap, bp, e, phi = ', x0, y0, ap, bp, e, phi)
    print(f'Center is at {int(x0 + 0.5)}, {int(y0 + 0.5)}')
    print(f'Semi-major axis is {ap}')
    print(f'Semi-minor axis is {bp}')
    print(f'Eccentricity is {e}')
    print(f'Rotation is {phi}')

    # plot
    if args.plot:
        plt.plot(x, y, 'x')     # given points
        x, y = ellipser.get_ellipse_pts((x0, y0, ap, bp, e, phi))
        plt.plot(x, y)
        plt.show()
